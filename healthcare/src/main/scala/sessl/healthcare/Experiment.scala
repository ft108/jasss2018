package sessl.healthcare

import org.jamesii.ml3.healthcare.HealthcareStateBuilder
import sessl._
import sessl.ml3._
import sessl.opt4j._
import sessl.optimization._

object Experiment extends App {
  minimize { (params, objective) =>
    execute(
      new Experiment with ParallelExecution with ParameterMaps with Observation with HealthCareCostObservation {
        model = "healthcare.ml3"
        simulator = NextReactionMethod()
        initializeWith(() => new HealthcareStateBuilder())
        parallelThreads = -1
        startTime = 1860
        stopTime = 2050
        replications = 10

        // load parameter maps from file
        fromFile("mortality.csv")("femaleMortalityRate", "maleMortalityRate")
        fromFile("careTransitionRates.csv")("femaleCareRate", "maleCareRate")
        fromFile("marriage.csv")("femaleMarriageRate", "maleMarriageRate")
        fromFile("move.csv")("moveOutFromParentsRate", "singleMoveRate", "familyMoveRate")
        fromFile("divorce.csv")("pastDivorceRate", "presentDivorceRate")
        fromFile("careTransitionStep.csv")("careTransition")

        // set parameters
        set("numCareLevels" <~ 5, "baseCareRate" <~ 0.0004)
        set("ageOfAdulthood" <~ 17)
        set("transitionYear" <~ 1965, "thePresent" <~ 2012)
        set("minPregnancyAge" <~ 17, "maxPregnancyAge" <~ 42)
        set("growingPopBirthRate" <~ 0.215, "steadyPopBirthRate" <~ 0.13)
        set("coupleMovesToExistingHousehold" <~ 0.3, "moveTogetherRate" <~ 0.3)
        set("agingParentsMoveInWithKids" <~ 0.1, "variableMoveBack" <~ 0.1)

        // set retirement age like the optimization demands
        set("ageOfRetirement" <~ params("ageOfRetirement"))

        // observation
        observeAt(range(2000, 1, 2050))
        val y = observe(healthCareCost)

        // calculate objective
        withReplicationsResult(result => {
          val allValues = result.runs.flatMap(_.values(y))
          objective <~ allValues.sum / allValues.size
        })
      })
  } using new Opt4JSetup {
    param("ageOfRetirement", 60.0, 100.0)
    optimizer = ParticleSwarmOptimization(iterations = 10, particles = 5)
    withOptimizationResults { results =>
      println("Overall results: " + results.head) //print results
    }
  }

}