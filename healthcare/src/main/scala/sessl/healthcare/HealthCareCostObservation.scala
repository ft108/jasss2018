package sessl.healthcare

import java.util.function.Consumer

import org.jamesii.core.util.misc.Pair
import org.jamesii.ml3.healthcare.HealthcareListener
import org.jamesii.ml3.observation.TimePointListObserver
import org.jamesii.ml3.simulator.simulators.ISimulator
import sessl.AbstractObservation.Observable
import sessl.TimeStampedData
import sessl.ml3._


/**
  * @author Tom Warnke
  */
trait HealthCareCostObservation {
  this: Experiment with Observation =>

  case object healthCareCost extends Observable[Double]

  instrumentations += ((runId: Int, _, simulator: ISimulator) => {

    val observer = {
      import scala.collection.JavaConverters._
      val jTimes = observationTimes.map(d => d.asInstanceOf[java.lang.Double]).asJava
      new TimePointListObserver(jTimes)
    }
    val listener = newGlobalCareCostListener(runId)

    observer.registerListener(listener)

    simulator.addObserver(observer)
  })

  private[this] def newGlobalCareCostListener(runId: Int): HealthcareListener = {
    val callback = new Consumer[Pair[java.lang.Double, HealthcareListener.CareDemandReport]] {
      override def accept(t: Pair[java.lang.Double, HealthcareListener.CareDemandReport]): Unit = {
        addValueFor(
          runId,
          healthCareCost,
          (t.getFirstValue, t.getSecondValue.getFormalCareCostPerTaxpayer).asInstanceOf[TimeStampedData[_]])
      }
    }
    new HealthcareListener(callback)
  }

}
